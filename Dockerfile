FROM nginx:latest

LABEL maintainer="schneider@hrz.uni-marburg.de"

# defined arguments with default values
ENV NGINX_HTTP=80
ENV NGINX_HTTPS=443
ENV NGINX_HTTPS_EXT=
ENV NGINX_SERVERNAME=_
ENV NGINX_ROOT=/var/www/html
ENV NGINX_DATA=/var/www/data
ENV NGINX_SSL_ONLY=0
ENV NGINX_SSL_CERTIFICATE=/etc/nginx/crt/fullchain.crt
ENV NGINX_SSL_KEY=/etc/nginx/crt/key.crt
ENV NGINX_FASTCGI=
ENV NGINX_FASTCGI_HOST=127.0.0.1
ENV NGINX_FASTCGI_PORT=9000
ENV NGINX_XAPI_PROXY_OPTIONS=0
ENV NGINX_POSTWOMAN=0

ENV TZ=Europe/Berlin
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

COPY default.template /tmp/default.template
COPY crt /etc/nginx/crt
COPY locations.template /tmp/locations.template
COPY fastcgi_location.template /tmp/fastcgi_location.template
COPY xapi_proxy_options /etc/nginx/
COPY postwoman_cors_header /etc/nginx/
COPY entrypoint_nginx /usr/bin/entrypoint_nginx

RUN mkdir -p /var/www/data && usermod -u 1000 nginx && chmod 755 /usr/bin/entrypoint_nginx

# substitue all the .env variables into the template files

CMD ["entrypoint_nginx"]



